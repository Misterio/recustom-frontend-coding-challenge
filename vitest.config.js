import { defineConfig } from 'vitest/config';

export default defineConfig({
    test: {
        // Test configuration options go here
        globals: true, // This option sets up the global test variables like `describe`, `it`, and `expect`
        environment: 'happy-dom',
        setupFiles: ['./setupTests.js'], // Optional: If you have a setup file
    },
});
