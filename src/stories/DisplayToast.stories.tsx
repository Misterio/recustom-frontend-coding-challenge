import React from "react";
import { Story, Meta } from "@storybook/react";
import {
  ToastCustom
} from "../modules/content-exploration/components/DisplayToast/DisplayToast";
import avatar from './assets/avatar.png';

export default {
  title: "Components/DisplayToast",
  component: ToastCustom,
} as Meta;

const Template: Story = ( args ) => <ToastCustom {...args} />;

export const Success = Template.bind({});
Success.args = {
  status: "success",
  content: "The action that you have done was a success! Well done",
};

export const SuccessCTA = Template.bind({});
SuccessCTA.args = {
  status: "success",
  title: 'Success',
  hasCTA: true,
  content: "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
};

export const Error = Template.bind({});
Error.args = {
  status: "error",
  content: "The file flowbite-figma-pro was permanently deleted.",
};

export const ErrorCTA = Template.bind({});
ErrorCTA.args = {
  status: "error",
  title: 'Attention',
  hasCTA: true,
  content: "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.",
};

export const Avatar = Template.bind({});
Avatar.args = {
  status: "avatar",
  title: "Bonnie Green",
  buttonText: "Button text",
  hasCTA: true,
  content: "Hi Neil, thanks for sharing your thoughts regardingFlowbite.",
  image: avatar
};
