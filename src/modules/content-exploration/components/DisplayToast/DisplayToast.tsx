"use client";

import React, { ReactNode } from "react";
import styles from "./DisplayToast.module.scss";
import Image from "next/image";
import { Button, Typography } from "@mui/material";
import CloseIcon from "./CloseIcon";
import { toast } from "react-toastify";
import cx from "classnames";

import checkF from "/public/assets/icons/check-f.svg";
import check from "/public/assets/icons/check.svg";
import notification from "/public/assets/icons/notification.svg";
import warning from "/public/assets/icons/warning-f.svg";

export interface DisplayToastProps {
  status: "success" | "error" | "avatar";
  content: string;
  title?: string;
  buttonText?: string;
  image?: string;
  hasCTA?: boolean;
  onInteraction?: () => void;
  isStory?: boolean;
}

const ctaIcons = {
  error: warning,
  success: checkF,
}

const statusIcons = {
  error: notification,
  success: check,
}

export const ToastCustom = ( {
                               status,
                               content,
                               title,
                               buttonText,
                               image,
                               hasCTA,
                               onInteraction
                             }: DisplayToastProps ) => {

  return (
    <div className={cx(styles.toast, styles[status])}>
      {image && <Image className={styles.img} src={image} alt={status}/>}
      <div className={styles.column}>
        {(hasCTA || title) && (
          <div className={styles.textSection}>
            {(status !== "avatar" && hasCTA) && <Image src={ctaIcons[status]} alt={status}/>}
            <Typography variant={'h6'} className={styles.title}>{title}</Typography>
          </div>
        )}
        <div className={styles.textSection}>
          {!(hasCTA || title) && <Image className={styles.imgWrapper} src={statusIcons[status]} alt={status}/>}
          <Typography variant="body2" className={styles.text}>
            {content}
          </Typography>
        </div>
        {hasCTA && (
          <Button variant="contained" color="primary" className={styles.btn} size="small" onClick={onInteraction}>
            <Typography variant="body4" className={styles.text}>
              {buttonText || "Take action"}
            </Typography>
          </Button>
        )}
      </div>
      <div className={styles.close}>
        <CloseIcon/>
      </div>
    </div>
  );
};

export default function DisplayToast( props: DisplayToastProps ): ReactNode {
  return toast(<ToastCustom {...props}/>, {position: "top-right"});
}
