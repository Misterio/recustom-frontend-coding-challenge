import React from 'react';
import { describe, it, expect, vi, beforeEach } from 'vitest';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import { ToastCustom } from './DisplayToast';

// Mocks for external components and assets
vi.mock('next/image', () => ({
    __esModule: true,
    default: (props) => {
        return <img {...props} />;
    },
}));

vi.mock('classnames', () => ({
    __esModule: true,
    default: (...classes) => classes.join(' '),
}));

describe('<ToastCustom />', () => {
    const defaultProps = {
        status: 'success',
        content: 'This is a success message!',
        title: 'Success',
        hasCTA: true,
        onInteraction: vi.fn(),
    };

    beforeEach(() => {
        vi.clearAllMocks();
    });

    it('renders the toast message correctly', () => {
        render(<ToastCustom {...defaultProps} />);

        expect(screen.getByText(defaultProps.content)).toBeInTheDocument();
        expect(screen.getByText(defaultProps.title)).toBeInTheDocument();
    });

    it('calls "onInteraction" when the CTA button is clicked', () => {
        render(<ToastCustom {...defaultProps} />);

        fireEvent.click(screen.getByText(/take action/i));
        expect(defaultProps.onInteraction).toHaveBeenCalled();
    });

    it('displays the correct status icon', () => {
        render(<ToastCustom {...defaultProps} />);

        const statusIcon = screen.getByAltText(defaultProps.status);
        expect(statusIcon).toBeInTheDocument();
    });

    // Add additional tests for error state, avatar state, and without CTA button...
});
