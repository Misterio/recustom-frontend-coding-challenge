import DisplayToast, {DisplayToastProps} from "@/modules/content-exploration/components/DisplayToast/DisplayToast";

export const toast = {
  success: (props: DisplayToastProps) => DisplayToast({ status: "success", ...props }),
  error: (props: DisplayToastProps) => DisplayToast({ status: "error", ...props }),
  avatar: (props: DisplayToastProps) => DisplayToast({ status: "avatar", ...props }),
};
