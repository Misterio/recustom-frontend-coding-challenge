import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { ApolloProvider } from "@/providers/apollo";
import { ToastContainer } from "react-toastify";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Recustom",
  description: "Recustom",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <ApolloProvider>{children}</ApolloProvider>
        <ToastContainer
          style={{ zIndex: 999999 }}
          position="top-center"
          autoClose={4000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          className="resetToast"
          limit={1}
        />
      </body>
    </html>
  );
}
